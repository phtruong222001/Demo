// khởi chạu server node
import jwt from 'jsonwebtoken';
import express from 'express';
import dotenv from 'dotenv'

dotenv.config() // cau hinnh
const app = express()
const PORT = process.env.PORT || 5000;

// server chayj len thanh cong thi console

//nhan du lieu dc nhan client
app.use(express.json());
const books = [
    {
        id:1,
        name:"Truong",
        age: 20
    },
    {
        id:2,
        name:"Nam",
        age:21
    }
]
// authentication
app.post('/login', (req, res)=>{
    // client submit thi lay thong tin submit
    const data = req.body  // lay da thong tin sumbmit len
    const accessToken = jwt.sign(data, process.env.ACCESS_TOKEN_SECRET,{
        expiresIn:"60"}); //expiresIn dat thoi han cho token
    res.json({accessToken}); //tra lai thong tin ve client
})

// xac thuc nguoi dung co hop lej k

// truoc khi login phai qua func
function authenToken(req,res, next){
    const authorizationHeader = req.header['authorization']
} 

//add duong dan
app.get('/books', (req, res) =>{
    // tra ve,           SECRET la bi mat
    res.json({ status : 'Success(Thanh cong)', data:books})
})


app.listen(PORT, () =>{
    console.log(`server is running on PORT ${PORT}`)
})

