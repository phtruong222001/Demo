import { Button } from 'antd'
import React from 'react'
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';
import { useEffect, useState } from 'react/cjs/react.development';


function Home(){
    const [profile, setProfile]=useState({
        fullname:"",
        email:""
    })
    let history = useHistory();
    const logout =() =>{
        history.push("/")
        localStorage.removeItem("refresh_token")   
    }
    // console.log("token", localStorage.getItem("access_token"))
    // console.log(`Bearer ${localStorage.getItem("access_token")}`)

    const refreshToken = () =>{
        var myHeaders = new Headers();
        myHeaders.append("Client-Id", "1");
        myHeaders.append("Client-Secret", "3c3adab39aeb093935e870c35cc5746c");
        myHeaders.append("Authorization", `Bearer ${localStorage.getItem("refresh_token")}`);

        var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        redirect: 'follow'
        };

        fetch("https://dev-api.trainery.live/v1/refresh", requestOptions)
        .then(response => response.json())
        .then(result => {console.log(result);
            console.log("day la access_token", result.access_token)
            localStorage.setItem("access_token", result.access_token)
        })
        .catch(error => console.log('error', error));
    }

    

    const getData=()=>{
        var myHeaders = new Headers();
        myHeaders.append("Client-Id", "1");
        myHeaders.append("Client-Secret", "3c3adab39aeb093935e870c35cc5746c");
        myHeaders.append("Authorization",`Bearer ${localStorage.getItem("access_token")}`);
        var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
        };
        fetch("https://dev-api.trainery.live/v1/customer", requestOptions)
        .then(response => response.json())
        .then(result => {console.log(result)
            const {data} = result
            setProfile({
                fullname :data.fullname,
                email: data.email,
            })
            // console.log("name" , data.fullname)
        })
        
        .catch(error => console.log('error', error));
    }

    useEffect(()=>{
        refreshToken();
        getData();
        },[])

    return(
        < >
            <h1> Welcome {profile.fullname} !!! </h1>
            <p> Email: {profile.email} </p>
            <Button onClick={logout}> Logout  </Button>
        </>
    )
}

export default Home
